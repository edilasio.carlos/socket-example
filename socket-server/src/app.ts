/**
 * https://www.digitalocean.com/community/tutorials/angular-socket-io
 */


import express from "express";
import http from "http";
import { Server } from "socket.io";


const app = express();
const httpServer = new http.Server(app);
const io = new Server(httpServer, {
    cors: {
        origin: "http://localhost:4200",
        methods: ["GET", "POST"],
        // credentials: true
    }
});

const documents: any = {};

io.on("connection", socket => {
    let previousId: any;

    const safeJoin = (currentId: any) => {
        socket.leave(previousId);
        socket.join(currentId);
        console.log(`Socket ${socket.id} joined room ${currentId}`)
        previousId = currentId;
    }

    socket.on("getDoc", docId => {
        safeJoin(docId);
        socket.emit("document", documents[docId]);
    });

    socket.on("addDoc", doc => {
        documents[doc.id] = doc;
        safeJoin(doc.id);
        io.emit("documents", Object.keys(documents));
        socket.emit("document", doc);
      });

    socket.on("editDoc", doc => {
        documents[doc.id] = doc;
        socket.to(doc.id).emit("document", doc);
    });

    io.emit("documents", Object.keys(documents));
    console.log(`Socket ${socket.id} has connected`);
});

// // enable CORS without external module
// io.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
//   });

httpServer.listen(4444, () => {
    console.log("App listening on port 4444 😃")
})